# phpword2vec
php调用word2vec实现机器学习
### 使用方法
    执行make进行编译
    执行phpphpword2vec.php可以得到当前关键词的文档向量（该工具是把300维向量转化文档向量的工具）
    php直接调用然后可以进行svm等分类操作
    该工具在已经有训练数据后调用
### 项目地址
    github：https://github.com/qieangel2013/phpword2vec
    oschina：https://gitee.com/qieangel2013/phpword2vec
    
